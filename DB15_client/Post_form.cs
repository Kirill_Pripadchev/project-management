﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class Post_form : Form
    {
        public Post_form()
        {
            InitializeComponent();
        }

        private void Post_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dB15DataSet.Post". При необходимости она может быть перемещена или удалена.
            //this.postTableAdapter.Fill(this.dB15DataSet.Post);
            Post_boss_checkBox.Checked = false;
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            conn.Open();
            string query = "select * from Post";
            SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
            adpt.Fill(this.dB15DataSet.Post);
            conn.Close();

        }

        private void Insert_button_Click(object sender, EventArgs e)
        {
            if (Post_id_box.Text != "Post id" && Department_id_box.Text != "Department id" && Post_name_box.Text != "Post name" && Post_salary_box.Text != "Post salary")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                string query = "";
                SqlConnection conn = new SqlConnection(connection_source);
                try 
                {
                    conn.Open();
                    if (Post_boss_checkBox.Checked == true)
                    {
                        query = "insert into Post (Post_id, Department_id, Post_name, Post_salary, Post_boss) values (" + Post_id_box.Text + ", " + Department_id_box.Text + ", '" + Post_name_box.Text + "', " + Post_salary_box.Text + ", 'true')";
                    }
                    else
                    {
                        query = "insert into Post (Post_id, Department_id, Post_name, Post_salary, Post_boss) values (" + Post_id_box.Text + ", " + Department_id_box.Text + ", '" + Post_name_box.Text + "', " + Post_salary_box.Text + ", 'false')";
                    }
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Post);
                    query = "select * from Post";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Post);
               //     this.postTableAdapter.Fill(this.dB15DataSet.Post);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Post_id_box.Text = "Post id";
                Department_id_box.Text = "Department id";
                Post_name_box.Text = "Post name";
                Post_salary_box.Text = "post salary";
                Post_boss_checkBox.Checked = false;
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Update_button_Click(object sender, EventArgs e)
        {
            if (Post_id_box.Text != "Post id" && Department_id_box.Text != "Department id" && Post_name_box.Text != "Post name" && Post_salary_box.Text != "Post salary") 
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try 
                {
                    conn.Open();
                    string row = Convert.ToString(dataGridView_post.CurrentRow);
                    string row_str = "";
                    char[] arr = row.ToCharArray();
                    foreach (char h in arr)
                    {
                        if (char.IsDigit(h) == true)
                        {
                            row_str += h;
                        }
                    }
                    int row_num = Convert.ToInt32(row_str);
                    string query = "";
                    if (Post_boss_checkBox.Checked == true)
                    {
                        query = "update Post set Post_id=" + Post_id_box.Text + ", Department_id=" + Department_id_box.Text + ", Post_name='" + Post_name_box.Text + "', Post_salary=" + Post_salary_box.Text + ", Post_boss='true' where Post_id=" + dataGridView_post.Rows[row_num].Cells[0].Value;
                    }
                    else
                    {
                        query = "update Post set Post_id=" + Post_id_box.Text + ", Department_id=" + Department_id_box.Text + ", Post_name='" + Post_name_box.Text + "', Post_salary=" + Post_salary_box.Text + ", Post_boss='false' where Post_id=" + dataGridView_post.Rows[row_num].Cells[0].Value;
                    }
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Post);
                    query = "select * from Post";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Post);
                //    this.postTableAdapter.Fill(this.dB15DataSet.Post);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Post_id_box.Text = "Post id";
                Department_id_box.Text = "Department id";
                Post_name_box.Text = "Post name";
                Post_salary_box.Text = "post salary";
                Post_boss_checkBox.Checked = false;
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            try
            {
                conn.Open();
                string row = Convert.ToString(dataGridView_post.CurrentRow);
                string row_str = "";
                char[] arr = row.ToCharArray();
                foreach (char h in arr)
                {
                    if (char.IsDigit(h) == true)
                    {
                        row_str += h;
                    }
                }
                int row_num = Convert.ToInt32(row_str);
                string query = "delete from Post where Post_id=" + dataGridView_post.Rows[row_num].Cells[0].Value;
                SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                adpt.Fill(this.dB15DataSet.Post);
                query = "select * from Post";
                SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                adpt2.Fill(this.dB15DataSet.Post);
            //    this.postTableAdapter.Fill(this.dB15DataSet.Post);
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
