﻿namespace DB15_client
{
    partial class Contract_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_contract = new System.Windows.Forms.DataGridView();
            this.contractidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workeridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contractbeginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contractendDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalcontractdurationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.contractTableAdapter = new DB15_client.DB15DataSetTableAdapters.ContractTableAdapter();
            this.Contract_id_box = new System.Windows.Forms.TextBox();
            this.Post_id_box = new System.Windows.Forms.TextBox();
            this.Worker_id_box = new System.Windows.Forms.TextBox();
            this.Contract_begin_box = new System.Windows.Forms.TextBox();
            this.Contract_end_box = new System.Windows.Forms.TextBox();
            this.Insert_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Delete_button = new System.Windows.Forms.Button();
            this.Contract_end_checkBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_contract
            // 
            this.dataGridView_contract.AutoGenerateColumns = false;
            this.dataGridView_contract.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_contract.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.contractidDataGridViewTextBoxColumn,
            this.postidDataGridViewTextBoxColumn,
            this.workeridDataGridViewTextBoxColumn,
            this.contractbeginDataGridViewTextBoxColumn,
            this.contractendDataGridViewTextBoxColumn,
            this.totalcontractdurationDataGridViewTextBoxColumn});
            this.dataGridView_contract.DataSource = this.contractBindingSource;
            this.dataGridView_contract.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_contract.Name = "dataGridView_contract";
            this.dataGridView_contract.ReadOnly = true;
            this.dataGridView_contract.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_contract.Size = new System.Drawing.Size(671, 319);
            this.dataGridView_contract.TabIndex = 0;
            // 
            // contractidDataGridViewTextBoxColumn
            // 
            this.contractidDataGridViewTextBoxColumn.DataPropertyName = "Contract_id";
            this.contractidDataGridViewTextBoxColumn.HeaderText = "Contract_id";
            this.contractidDataGridViewTextBoxColumn.Name = "contractidDataGridViewTextBoxColumn";
            this.contractidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // postidDataGridViewTextBoxColumn
            // 
            this.postidDataGridViewTextBoxColumn.DataPropertyName = "Post_id";
            this.postidDataGridViewTextBoxColumn.HeaderText = "Post_id";
            this.postidDataGridViewTextBoxColumn.Name = "postidDataGridViewTextBoxColumn";
            this.postidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // workeridDataGridViewTextBoxColumn
            // 
            this.workeridDataGridViewTextBoxColumn.DataPropertyName = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.HeaderText = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.Name = "workeridDataGridViewTextBoxColumn";
            this.workeridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contractbeginDataGridViewTextBoxColumn
            // 
            this.contractbeginDataGridViewTextBoxColumn.DataPropertyName = "Contract_begin";
            this.contractbeginDataGridViewTextBoxColumn.HeaderText = "Contract_begin";
            this.contractbeginDataGridViewTextBoxColumn.Name = "contractbeginDataGridViewTextBoxColumn";
            this.contractbeginDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contractendDataGridViewTextBoxColumn
            // 
            this.contractendDataGridViewTextBoxColumn.DataPropertyName = "Contract_end";
            this.contractendDataGridViewTextBoxColumn.HeaderText = "Contract_end";
            this.contractendDataGridViewTextBoxColumn.Name = "contractendDataGridViewTextBoxColumn";
            this.contractendDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // totalcontractdurationDataGridViewTextBoxColumn
            // 
            this.totalcontractdurationDataGridViewTextBoxColumn.DataPropertyName = "Total_contract_duration";
            this.totalcontractdurationDataGridViewTextBoxColumn.HeaderText = "Total_contract_duration";
            this.totalcontractdurationDataGridViewTextBoxColumn.Name = "totalcontractdurationDataGridViewTextBoxColumn";
            this.totalcontractdurationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contractBindingSource
            // 
            this.contractBindingSource.DataMember = "Contract";
            this.contractBindingSource.DataSource = this.dB15DataSet;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // contractTableAdapter
            // 
            this.contractTableAdapter.ClearBeforeFill = true;
            // 
            // Contract_id_box
            // 
            this.Contract_id_box.Location = new System.Drawing.Point(677, 12);
            this.Contract_id_box.Name = "Contract_id_box";
            this.Contract_id_box.Size = new System.Drawing.Size(140, 20);
            this.Contract_id_box.TabIndex = 1;
            this.Contract_id_box.Text = "Contract id";
            // 
            // Post_id_box
            // 
            this.Post_id_box.Location = new System.Drawing.Point(677, 38);
            this.Post_id_box.Name = "Post_id_box";
            this.Post_id_box.Size = new System.Drawing.Size(140, 20);
            this.Post_id_box.TabIndex = 2;
            this.Post_id_box.Text = "Post id";
            // 
            // Worker_id_box
            // 
            this.Worker_id_box.Location = new System.Drawing.Point(677, 64);
            this.Worker_id_box.Name = "Worker_id_box";
            this.Worker_id_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_id_box.TabIndex = 3;
            this.Worker_id_box.Text = "Worker id";
            // 
            // Contract_begin_box
            // 
            this.Contract_begin_box.Location = new System.Drawing.Point(677, 90);
            this.Contract_begin_box.Name = "Contract_begin_box";
            this.Contract_begin_box.Size = new System.Drawing.Size(140, 20);
            this.Contract_begin_box.TabIndex = 4;
            this.Contract_begin_box.Text = "Contract begin";
            // 
            // Contract_end_box
            // 
            this.Contract_end_box.Location = new System.Drawing.Point(677, 116);
            this.Contract_end_box.Name = "Contract_end_box";
            this.Contract_end_box.Size = new System.Drawing.Size(140, 20);
            this.Contract_end_box.TabIndex = 5;
            this.Contract_end_box.Text = "Contract end";
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(677, 142);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 6;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(677, 198);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 7;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(677, 254);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 8;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Contract_end_checkBox
            // 
            this.Contract_end_checkBox.AutoSize = true;
            this.Contract_end_checkBox.Location = new System.Drawing.Point(823, 119);
            this.Contract_end_checkBox.Name = "Contract_end_checkBox";
            this.Contract_end_checkBox.Size = new System.Drawing.Size(15, 14);
            this.Contract_end_checkBox.TabIndex = 9;
            this.Contract_end_checkBox.UseVisualStyleBackColor = true;
            this.Contract_end_checkBox.CheckedChanged += new System.EventHandler(this.Contract_end_checkBox_CheckedChanged);
            // 
            // Contract_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 319);
            this.Controls.Add(this.Contract_end_checkBox);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.Contract_end_box);
            this.Controls.Add(this.Contract_begin_box);
            this.Controls.Add(this.Worker_id_box);
            this.Controls.Add(this.Post_id_box);
            this.Controls.Add(this.Contract_id_box);
            this.Controls.Add(this.dataGridView_contract);
            this.Name = "Contract_form";
            this.Text = "Договор";
            this.Load += new System.EventHandler(this.Contract_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_contract;
        private DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource contractBindingSource;
        private DB15DataSetTableAdapters.ContractTableAdapter contractTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn contractidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn postidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workeridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contractbeginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contractendDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalcontractdurationDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox Contract_id_box;
        private System.Windows.Forms.TextBox Post_id_box;
        private System.Windows.Forms.TextBox Worker_id_box;
        private System.Windows.Forms.TextBox Contract_begin_box;
        private System.Windows.Forms.TextBox Contract_end_box;
        private System.Windows.Forms.Button Insert_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Delete_button;
        private System.Windows.Forms.CheckBox Contract_end_checkBox;
    }
}