﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class Worker_form : Form
    {
        AddWorkerPlace_form awp_fm = new AddWorkerPlace_form();
        public Worker_form()
        {
            InitializeComponent();
        }

        private void Worker_form_Load(object sender, EventArgs e)
        {
            awp_fm.Owner = this;
            AddOwnedForm(awp_fm);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dB15DataSet.Worker". При необходимости она может быть перемещена или удалена.
            Form1 main = this.Owner as Form1;
            label1.Text = main.label1.Text;
            label2.Text = main.label2.Text;
            label3.Text = main.label3.Text;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            conn.Open();
            string query = "select * from Worker";
            SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
            //this.workerTableAdapter.Fill(this.dB15DataSet.Worker);
            adpt.Fill(this.dB15DataSet.Worker);
            conn.Close();
        }

        private void Update_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Worker_name_box.Text != "Worker name" && Worker_birthday_box.Text != "Worker birthday" && Worker_sex_box.Text != "Worker sex" && Worker_experience_box.Text != "Worker experience")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string row = Convert.ToString(dataGridView_worker.CurrentRow);
                    string row_str = "";
                    char[] arr = row.ToCharArray();
                    foreach (char h in arr)
                    {   
                        if (char.IsDigit(h) == true)
                        {
                            row_str += h;
                        }
                    }
                    int row_num = Convert.ToInt32(row_str);
                    string query = "update Worker set Worker_id=" + Worker_id_box.Text + ", Worker_name='" + Worker_name_box.Text + "', Worker_birthday='" + Worker_birthday_box.Text + "', Worker_sex='" + Worker_sex_box.Text + "', Worker_experience=" + Worker_experience_box.Text + " where Worker_id=" + dataGridView_worker.Rows[row_num].Cells[0].Value;
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Worker);
                    query = "select * from Worker";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Worker);
                    //this.workerTableAdapter.Fill(this.dB15DataSet.Worker);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Worker_name_box.Text = "Worker name";
                Worker_birthday_box.Text = "Worker birthday";
                Worker_sex_box.Text = "Worker sex";
                Worker_experience_box.Text = "Worker experience";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Insert_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Worker_name_box.Text != "Worker name" && Worker_birthday_box.Text != "Worker birthday" && Worker_sex_box.Text != "Worker sex" && Worker_experience_box.Text != "Worker experience")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string query = "insert into Worker (Worker_id, Worker_name, Worker_birthday, Worker_sex, Worker_experience) values (" + Worker_id_box.Text + ", '" + Worker_name_box.Text + "', '" + Worker_birthday_box.Text + "', '" + Worker_sex_box.Text + "', " + Worker_experience_box.Text + ")";
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Worker);
                    query = "select * from Worker";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Worker);
                    //this.workerTableAdapter.Fill(this.dB15DataSet.Worker);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Worker_name_box.Text = "Worker name";
                Worker_birthday_box.Text = "Worker birthday";
                Worker_sex_box.Text = "Worker sex";
                Worker_experience_box.Text = "Worker experience";
            }
            else 
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            try
            {
                conn.Open();
                string row = Convert.ToString(dataGridView_worker.CurrentRow);
                string row_str = "";
                char[] arr = row.ToCharArray();
                foreach (char h in arr)
                {
                    if (char.IsDigit(h) == true)
                    {
                        row_str += h;
                    }
                }
                int row_num = Convert.ToInt32(row_str);
                string query = "delete from Worker where Worker_id="+dataGridView_worker.Rows[row_num].Cells[0].Value;
                SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                adpt.Fill(this.dB15DataSet.Worker);
                query = "select * from Worker";
                SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                adpt2.Fill(this.dB15DataSet.Worker);
                //this.workerTableAdapter.Fill(this.dB15DataSet.Worker);
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddWorkerPlace_button_Click(object sender, EventArgs e)
        {
            awp_fm.ShowDialog();
        }
    }
}
