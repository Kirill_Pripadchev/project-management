﻿namespace DB15_client
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Worker_button = new System.Windows.Forms.Button();
            this.Contract_button = new System.Windows.Forms.Button();
            this.Department_button = new System.Windows.Forms.Button();
            this.Post_button = new System.Windows.Forms.Button();
            this.Vacation_button = new System.Windows.Forms.Button();
            this.Desease_button = new System.Windows.Forms.Button();
            this.Business_trip_button = new System.Windows.Forms.Button();
            this.Characteristic_button = new System.Windows.Forms.Button();
            this.Path_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Report_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Worker_button
            // 
            this.Worker_button.Location = new System.Drawing.Point(12, 12);
            this.Worker_button.Name = "Worker_button";
            this.Worker_button.Size = new System.Drawing.Size(120, 40);
            this.Worker_button.TabIndex = 0;
            this.Worker_button.Text = "Сотрудник";
            this.Worker_button.UseVisualStyleBackColor = true;
            this.Worker_button.Click += new System.EventHandler(this.Worker_button_Click);
            // 
            // Contract_button
            // 
            this.Contract_button.Location = new System.Drawing.Point(143, 12);
            this.Contract_button.Name = "Contract_button";
            this.Contract_button.Size = new System.Drawing.Size(120, 40);
            this.Contract_button.TabIndex = 1;
            this.Contract_button.Text = "Договор";
            this.Contract_button.UseVisualStyleBackColor = true;
            this.Contract_button.Click += new System.EventHandler(this.Contract_button_Click);
            // 
            // Department_button
            // 
            this.Department_button.Location = new System.Drawing.Point(12, 58);
            this.Department_button.Name = "Department_button";
            this.Department_button.Size = new System.Drawing.Size(120, 40);
            this.Department_button.TabIndex = 2;
            this.Department_button.Text = "Отдел";
            this.Department_button.UseVisualStyleBackColor = true;
            this.Department_button.Click += new System.EventHandler(this.Department_button_Click);
            // 
            // Post_button
            // 
            this.Post_button.Location = new System.Drawing.Point(143, 58);
            this.Post_button.Name = "Post_button";
            this.Post_button.Size = new System.Drawing.Size(120, 40);
            this.Post_button.TabIndex = 3;
            this.Post_button.Text = "Должность";
            this.Post_button.UseVisualStyleBackColor = true;
            this.Post_button.Click += new System.EventHandler(this.Post_button_Click);
            // 
            // Vacation_button
            // 
            this.Vacation_button.Location = new System.Drawing.Point(12, 104);
            this.Vacation_button.Name = "Vacation_button";
            this.Vacation_button.Size = new System.Drawing.Size(120, 40);
            this.Vacation_button.TabIndex = 4;
            this.Vacation_button.Text = "Отпуск";
            this.Vacation_button.UseVisualStyleBackColor = true;
            this.Vacation_button.Click += new System.EventHandler(this.Vacation_button_Click);
            // 
            // Desease_button
            // 
            this.Desease_button.Location = new System.Drawing.Point(143, 104);
            this.Desease_button.Name = "Desease_button";
            this.Desease_button.Size = new System.Drawing.Size(120, 40);
            this.Desease_button.TabIndex = 5;
            this.Desease_button.Text = "Больничный";
            this.Desease_button.UseVisualStyleBackColor = true;
            this.Desease_button.Click += new System.EventHandler(this.Desease_button_Click);
            // 
            // Business_trip_button
            // 
            this.Business_trip_button.Location = new System.Drawing.Point(12, 150);
            this.Business_trip_button.Name = "Business_trip_button";
            this.Business_trip_button.Size = new System.Drawing.Size(120, 40);
            this.Business_trip_button.TabIndex = 6;
            this.Business_trip_button.Text = "Командировка";
            this.Business_trip_button.UseVisualStyleBackColor = true;
            this.Business_trip_button.Click += new System.EventHandler(this.Business_trip_button_Click);
            // 
            // Characteristic_button
            // 
            this.Characteristic_button.Location = new System.Drawing.Point(143, 150);
            this.Characteristic_button.Name = "Characteristic_button";
            this.Characteristic_button.Size = new System.Drawing.Size(120, 40);
            this.Characteristic_button.TabIndex = 7;
            this.Characteristic_button.Text = "Характеристика";
            this.Characteristic_button.UseVisualStyleBackColor = true;
            this.Characteristic_button.Click += new System.EventHandler(this.Characteristic_button_Click);
            // 
            // Path_button
            // 
            this.Path_button.Location = new System.Drawing.Point(143, 196);
            this.Path_button.Name = "Path_button";
            this.Path_button.Size = new System.Drawing.Size(120, 40);
            this.Path_button.TabIndex = 8;
            this.Path_button.Text = "Настройки";
            this.Path_button.UseVisualStyleBackColor = true;
            this.Path_button.Click += new System.EventHandler(this.Path_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 248);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(140, 275);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(213, 253);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "label3";
            this.label3.Visible = false;
            // 
            // Report_button
            // 
            this.Report_button.Location = new System.Drawing.Point(12, 196);
            this.Report_button.Name = "Report_button";
            this.Report_button.Size = new System.Drawing.Size(120, 40);
            this.Report_button.TabIndex = 12;
            this.Report_button.Text = "Отчет";
            this.Report_button.UseVisualStyleBackColor = true;
            this.Report_button.Click += new System.EventHandler(this.Report_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 298);
            this.Controls.Add(this.Report_button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Path_button);
            this.Controls.Add(this.Characteristic_button);
            this.Controls.Add(this.Business_trip_button);
            this.Controls.Add(this.Desease_button);
            this.Controls.Add(this.Vacation_button);
            this.Controls.Add(this.Post_button);
            this.Controls.Add(this.Department_button);
            this.Controls.Add(this.Contract_button);
            this.Controls.Add(this.Worker_button);
            this.MinimumSize = new System.Drawing.Size(291, 246);
            this.Name = "Form1";
            this.Text = "Data base client";
            this.Load += new System.EventHandler(this.Main_form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Worker_button;
        private System.Windows.Forms.Button Contract_button;
        private System.Windows.Forms.Button Department_button;
        private System.Windows.Forms.Button Post_button;
        private System.Windows.Forms.Button Vacation_button;
        private System.Windows.Forms.Button Desease_button;
        private System.Windows.Forms.Button Business_trip_button;
        private System.Windows.Forms.Button Characteristic_button;
        private System.Windows.Forms.Button Path_button;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Report_button;
    }
}

