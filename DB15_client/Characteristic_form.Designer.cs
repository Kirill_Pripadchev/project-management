﻿namespace DB15_client
{
    partial class Characteristic_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_characteristic = new System.Windows.Forms.DataGridView();
            this.workeridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.characteristicidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.characteristictextDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.characteristicdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.characteristicBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.characteristicTableAdapter = new DB15_client.DB15DataSetTableAdapters.CharacteristicTableAdapter();
            this.Worker_id_box = new System.Windows.Forms.TextBox();
            this.Characteristic_id_box = new System.Windows.Forms.TextBox();
            this.Characteristic_text_box = new System.Windows.Forms.TextBox();
            this.Characteristic_date_box = new System.Windows.Forms.TextBox();
            this.Delete_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Insert_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_characteristic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.characteristicBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_characteristic
            // 
            this.dataGridView_characteristic.AutoGenerateColumns = false;
            this.dataGridView_characteristic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_characteristic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workeridDataGridViewTextBoxColumn,
            this.characteristicidDataGridViewTextBoxColumn,
            this.characteristictextDataGridViewTextBoxColumn,
            this.characteristicdateDataGridViewTextBoxColumn});
            this.dataGridView_characteristic.DataSource = this.characteristicBindingSource;
            this.dataGridView_characteristic.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_characteristic.Name = "dataGridView_characteristic";
            this.dataGridView_characteristic.ReadOnly = true;
            this.dataGridView_characteristic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_characteristic.Size = new System.Drawing.Size(470, 289);
            this.dataGridView_characteristic.TabIndex = 0;
            // 
            // workeridDataGridViewTextBoxColumn
            // 
            this.workeridDataGridViewTextBoxColumn.DataPropertyName = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.HeaderText = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.Name = "workeridDataGridViewTextBoxColumn";
            this.workeridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // characteristicidDataGridViewTextBoxColumn
            // 
            this.characteristicidDataGridViewTextBoxColumn.DataPropertyName = "Characteristic_id";
            this.characteristicidDataGridViewTextBoxColumn.HeaderText = "Characteristic_id";
            this.characteristicidDataGridViewTextBoxColumn.Name = "characteristicidDataGridViewTextBoxColumn";
            this.characteristicidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // characteristictextDataGridViewTextBoxColumn
            // 
            this.characteristictextDataGridViewTextBoxColumn.DataPropertyName = "Characteristic_text";
            this.characteristictextDataGridViewTextBoxColumn.HeaderText = "Characteristic_text";
            this.characteristictextDataGridViewTextBoxColumn.Name = "characteristictextDataGridViewTextBoxColumn";
            this.characteristictextDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // characteristicdateDataGridViewTextBoxColumn
            // 
            this.characteristicdateDataGridViewTextBoxColumn.DataPropertyName = "Characteristic_date";
            this.characteristicdateDataGridViewTextBoxColumn.HeaderText = "Characteristic_date";
            this.characteristicdateDataGridViewTextBoxColumn.Name = "characteristicdateDataGridViewTextBoxColumn";
            this.characteristicdateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // characteristicBindingSource
            // 
            this.characteristicBindingSource.DataMember = "Characteristic";
            this.characteristicBindingSource.DataSource = this.dB15DataSet;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // characteristicTableAdapter
            // 
            this.characteristicTableAdapter.ClearBeforeFill = true;
            // 
            // Worker_id_box
            // 
            this.Worker_id_box.Location = new System.Drawing.Point(476, 12);
            this.Worker_id_box.Name = "Worker_id_box";
            this.Worker_id_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_id_box.TabIndex = 1;
            this.Worker_id_box.Text = "Worker id";
            // 
            // Characteristic_id_box
            // 
            this.Characteristic_id_box.Location = new System.Drawing.Point(476, 38);
            this.Characteristic_id_box.Name = "Characteristic_id_box";
            this.Characteristic_id_box.Size = new System.Drawing.Size(140, 20);
            this.Characteristic_id_box.TabIndex = 2;
            this.Characteristic_id_box.Text = "Characteristic id";
            // 
            // Characteristic_text_box
            // 
            this.Characteristic_text_box.Location = new System.Drawing.Point(476, 64);
            this.Characteristic_text_box.Name = "Characteristic_text_box";
            this.Characteristic_text_box.Size = new System.Drawing.Size(140, 20);
            this.Characteristic_text_box.TabIndex = 3;
            this.Characteristic_text_box.Text = "Characteristic text";
            // 
            // Characteristic_date_box
            // 
            this.Characteristic_date_box.Location = new System.Drawing.Point(476, 90);
            this.Characteristic_date_box.Name = "Characteristic_date_box";
            this.Characteristic_date_box.Size = new System.Drawing.Size(140, 20);
            this.Characteristic_date_box.TabIndex = 4;
            this.Characteristic_date_box.Text = "Characteristic date";
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(476, 228);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 11;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(476, 172);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 10;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(476, 116);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 9;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Характеристика
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 290);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.Characteristic_date_box);
            this.Controls.Add(this.Characteristic_text_box);
            this.Controls.Add(this.Characteristic_id_box);
            this.Controls.Add(this.Worker_id_box);
            this.Controls.Add(this.dataGridView_characteristic);
            this.Name = "Характеристика";
            this.Text = "Характеристика";
            this.Load += new System.EventHandler(this.Characteristic_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_characteristic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.characteristicBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_characteristic;
        private DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource characteristicBindingSource;
        private DB15DataSetTableAdapters.CharacteristicTableAdapter characteristicTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn workeridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn characteristicidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn characteristictextDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn characteristicdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox Worker_id_box;
        private System.Windows.Forms.TextBox Characteristic_id_box;
        private System.Windows.Forms.TextBox Characteristic_text_box;
        private System.Windows.Forms.TextBox Characteristic_date_box;
        private System.Windows.Forms.Button Delete_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Insert_button;
    }
}