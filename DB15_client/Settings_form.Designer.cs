﻿namespace DB15_client
{
    partial class Settings_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Data_source_box = new System.Windows.Forms.TextBox();
            this.Initial_catalog_box = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Integrated_security_box = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Data_source_box
            // 
            this.Data_source_box.Location = new System.Drawing.Point(12, 12);
            this.Data_source_box.Name = "Data_source_box";
            this.Data_source_box.Size = new System.Drawing.Size(140, 20);
            this.Data_source_box.TabIndex = 0;
            this.Data_source_box.Text = "Data Source";
            // 
            // Initial_catalog_box
            // 
            this.Initial_catalog_box.Location = new System.Drawing.Point(12, 38);
            this.Initial_catalog_box.Name = "Initial_catalog_box";
            this.Initial_catalog_box.Size = new System.Drawing.Size(140, 20);
            this.Initial_catalog_box.TabIndex = 1;
            this.Initial_catalog_box.Text = "Initial Catalog";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 40);
            this.button1.TabIndex = 2;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Integrated_security_box
            // 
            this.Integrated_security_box.Location = new System.Drawing.Point(12, 64);
            this.Integrated_security_box.Name = "Integrated_security_box";
            this.Integrated_security_box.Size = new System.Drawing.Size(140, 20);
            this.Integrated_security_box.TabIndex = 3;
            this.Integrated_security_box.Text = "Integrated Security";
            // 
            // Settings_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(167, 144);
            this.Controls.Add(this.Integrated_security_box);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Initial_catalog_box);
            this.Controls.Add(this.Data_source_box);
            this.Name = "Settings_form";
            this.Text = "Settings_form";
            this.Load += new System.EventHandler(this.Settings_form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Data_source_box;
        private System.Windows.Forms.TextBox Initial_catalog_box;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Integrated_security_box;
    }
}