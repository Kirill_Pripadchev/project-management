﻿namespace DB15_client
{
    partial class Report_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DB15DataSet = new DB15_client.DB15DataSet();
            this.WorkerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.WorkerTableAdapter = new DB15_client.DB15DataSetTableAdapters.WorkerTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.DB15DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.WorkerBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "DB15_client.Report.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(650, 467);
            this.reportViewer1.TabIndex = 0;
            // 
            // DB15DataSet
            // 
            this.DB15DataSet.DataSetName = "DB15DataSet";
            this.DB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // WorkerBindingSource
            // 
            this.WorkerBindingSource.DataMember = "Worker";
            this.WorkerBindingSource.DataSource = this.DB15DataSet;
            // 
            // WorkerTableAdapter
            // 
            this.WorkerTableAdapter.ClearBeforeFill = true;
            // 
            // Report_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 467);
            this.Controls.Add(this.reportViewer1);
            this.Name = "Report_form";
            this.Text = "Report_form";
            this.Load += new System.EventHandler(this.Report_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DB15DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkerBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource WorkerBindingSource;
        private DB15DataSet DB15DataSet;
        private DB15DataSetTableAdapters.WorkerTableAdapter WorkerTableAdapter;
    }
}