﻿namespace DB15_client
{
    partial class Business_trip_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_business_trip = new System.Windows.Forms.DataGridView();
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.businesstripBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.business_tripTableAdapter = new DB15_client.DB15DataSetTableAdapters.Business_tripTableAdapter();
            this.workeridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.businesstripidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.businesstripbeginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.businesstripendDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.businesstrippurposeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.businesstrippointDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.businesstrippaymentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Business_trip_id_box = new System.Windows.Forms.TextBox();
            this.Worker_id_box = new System.Windows.Forms.TextBox();
            this.Business_trip_begin_box = new System.Windows.Forms.TextBox();
            this.Business_trip_end_box = new System.Windows.Forms.TextBox();
            this.Business_trip_purpose_box = new System.Windows.Forms.TextBox();
            this.Business_trip_point_box = new System.Windows.Forms.TextBox();
            this.Business_trip_payment_box = new System.Windows.Forms.TextBox();
            this.Delete_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Insert_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_business_trip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.businesstripBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_business_trip
            // 
            this.dataGridView_business_trip.AutoGenerateColumns = false;
            this.dataGridView_business_trip.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_business_trip.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workeridDataGridViewTextBoxColumn,
            this.businesstripidDataGridViewTextBoxColumn,
            this.businesstripbeginDataGridViewTextBoxColumn,
            this.businesstripendDataGridViewTextBoxColumn,
            this.businesstrippurposeDataGridViewTextBoxColumn,
            this.businesstrippointDataGridViewTextBoxColumn,
            this.businesstrippaymentDataGridViewTextBoxColumn});
            this.dataGridView_business_trip.DataSource = this.businesstripBindingSource;
            this.dataGridView_business_trip.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_business_trip.Name = "dataGridView_business_trip";
            this.dataGridView_business_trip.ReadOnly = true;
            this.dataGridView_business_trip.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_business_trip.Size = new System.Drawing.Size(766, 368);
            this.dataGridView_business_trip.TabIndex = 0;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // businesstripBindingSource
            // 
            this.businesstripBindingSource.DataMember = "Business_trip";
            this.businesstripBindingSource.DataSource = this.dB15DataSet;
            // 
            // business_tripTableAdapter
            // 
            this.business_tripTableAdapter.ClearBeforeFill = true;
            // 
            // workeridDataGridViewTextBoxColumn
            // 
            this.workeridDataGridViewTextBoxColumn.DataPropertyName = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.HeaderText = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.Name = "workeridDataGridViewTextBoxColumn";
            this.workeridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // businesstripidDataGridViewTextBoxColumn
            // 
            this.businesstripidDataGridViewTextBoxColumn.DataPropertyName = "Business_trip_id";
            this.businesstripidDataGridViewTextBoxColumn.HeaderText = "Business_trip_id";
            this.businesstripidDataGridViewTextBoxColumn.Name = "businesstripidDataGridViewTextBoxColumn";
            this.businesstripidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // businesstripbeginDataGridViewTextBoxColumn
            // 
            this.businesstripbeginDataGridViewTextBoxColumn.DataPropertyName = "Business_trip_begin";
            this.businesstripbeginDataGridViewTextBoxColumn.HeaderText = "Business_trip_begin";
            this.businesstripbeginDataGridViewTextBoxColumn.Name = "businesstripbeginDataGridViewTextBoxColumn";
            this.businesstripbeginDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // businesstripendDataGridViewTextBoxColumn
            // 
            this.businesstripendDataGridViewTextBoxColumn.DataPropertyName = "Business_trip_end";
            this.businesstripendDataGridViewTextBoxColumn.HeaderText = "Business_trip_end";
            this.businesstripendDataGridViewTextBoxColumn.Name = "businesstripendDataGridViewTextBoxColumn";
            this.businesstripendDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // businesstrippurposeDataGridViewTextBoxColumn
            // 
            this.businesstrippurposeDataGridViewTextBoxColumn.DataPropertyName = "Business_trip_purpose";
            this.businesstrippurposeDataGridViewTextBoxColumn.HeaderText = "Business_trip_purpose";
            this.businesstrippurposeDataGridViewTextBoxColumn.Name = "businesstrippurposeDataGridViewTextBoxColumn";
            this.businesstrippurposeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // businesstrippointDataGridViewTextBoxColumn
            // 
            this.businesstrippointDataGridViewTextBoxColumn.DataPropertyName = "Business_trip_point";
            this.businesstrippointDataGridViewTextBoxColumn.HeaderText = "Business_trip_point";
            this.businesstrippointDataGridViewTextBoxColumn.Name = "businesstrippointDataGridViewTextBoxColumn";
            this.businesstrippointDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // businesstrippaymentDataGridViewTextBoxColumn
            // 
            this.businesstrippaymentDataGridViewTextBoxColumn.DataPropertyName = "Business_trip_payment";
            this.businesstrippaymentDataGridViewTextBoxColumn.HeaderText = "Business_trip_payment";
            this.businesstrippaymentDataGridViewTextBoxColumn.Name = "businesstrippaymentDataGridViewTextBoxColumn";
            this.businesstrippaymentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Business_trip_id_box
            // 
            this.Business_trip_id_box.Location = new System.Drawing.Point(772, 38);
            this.Business_trip_id_box.Name = "Business_trip_id_box";
            this.Business_trip_id_box.Size = new System.Drawing.Size(140, 20);
            this.Business_trip_id_box.TabIndex = 2;
            this.Business_trip_id_box.Text = "Business trip id";
            // 
            // Worker_id_box
            // 
            this.Worker_id_box.Location = new System.Drawing.Point(772, 12);
            this.Worker_id_box.Name = "Worker_id_box";
            this.Worker_id_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_id_box.TabIndex = 1;
            this.Worker_id_box.Text = "Worker id";
            // 
            // Business_trip_begin_box
            // 
            this.Business_trip_begin_box.Location = new System.Drawing.Point(772, 64);
            this.Business_trip_begin_box.Name = "Business_trip_begin_box";
            this.Business_trip_begin_box.Size = new System.Drawing.Size(140, 20);
            this.Business_trip_begin_box.TabIndex = 3;
            this.Business_trip_begin_box.Text = "Business trip begin";
            // 
            // Business_trip_end_box
            // 
            this.Business_trip_end_box.Location = new System.Drawing.Point(772, 90);
            this.Business_trip_end_box.Name = "Business_trip_end_box";
            this.Business_trip_end_box.Size = new System.Drawing.Size(140, 20);
            this.Business_trip_end_box.TabIndex = 4;
            this.Business_trip_end_box.Text = "Business trip end";
            // 
            // Business_trip_purpose_box
            // 
            this.Business_trip_purpose_box.Location = new System.Drawing.Point(772, 116);
            this.Business_trip_purpose_box.Name = "Business_trip_purpose_box";
            this.Business_trip_purpose_box.Size = new System.Drawing.Size(140, 20);
            this.Business_trip_purpose_box.TabIndex = 5;
            this.Business_trip_purpose_box.Text = "Busines trip purpose";
            // 
            // Business_trip_point_box
            // 
            this.Business_trip_point_box.Location = new System.Drawing.Point(772, 142);
            this.Business_trip_point_box.Name = "Business_trip_point_box";
            this.Business_trip_point_box.Size = new System.Drawing.Size(140, 20);
            this.Business_trip_point_box.TabIndex = 6;
            this.Business_trip_point_box.Text = "Business trip point";
            // 
            // Business_trip_payment_box
            // 
            this.Business_trip_payment_box.Location = new System.Drawing.Point(772, 168);
            this.Business_trip_payment_box.Name = "Business_trip_payment_box";
            this.Business_trip_payment_box.Size = new System.Drawing.Size(140, 20);
            this.Business_trip_payment_box.TabIndex = 7;
            this.Business_trip_payment_box.Text = "Business trip payment";
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(772, 306);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 11;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(772, 250);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 10;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(772, 194);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 9;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Business_trip_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 368);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.Business_trip_payment_box);
            this.Controls.Add(this.Business_trip_point_box);
            this.Controls.Add(this.Business_trip_purpose_box);
            this.Controls.Add(this.Business_trip_end_box);
            this.Controls.Add(this.Business_trip_begin_box);
            this.Controls.Add(this.Worker_id_box);
            this.Controls.Add(this.Business_trip_id_box);
            this.Controls.Add(this.dataGridView_business_trip);
            this.Name = "Business_trip_form";
            this.Text = "Командировка";
            this.Load += new System.EventHandler(this.Business_trip_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_business_trip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.businesstripBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_business_trip;
        private DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource businesstripBindingSource;
        private DB15DataSetTableAdapters.Business_tripTableAdapter business_tripTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn workeridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn businesstripidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn businesstripbeginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn businesstripendDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn businesstrippurposeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn businesstrippointDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn businesstrippaymentDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox Business_trip_id_box;
        private System.Windows.Forms.TextBox Worker_id_box;
        private System.Windows.Forms.TextBox Business_trip_begin_box;
        private System.Windows.Forms.TextBox Business_trip_end_box;
        private System.Windows.Forms.TextBox Business_trip_purpose_box;
        private System.Windows.Forms.TextBox Business_trip_point_box;
        private System.Windows.Forms.TextBox Business_trip_payment_box;
        private System.Windows.Forms.Button Delete_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Insert_button;
    }
}