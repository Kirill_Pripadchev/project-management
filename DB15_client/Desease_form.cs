﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class Desease_form : Form
    {
        public Desease_form()
        {
            InitializeComponent();
        }

        private void Desease_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dB15DataSet.Desease". При необходимости она может быть перемещена или удалена.
            //this.deseaseTableAdapter.Fill(this.dB15DataSet.Desease);
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            conn.Open();
            string query = "select * from Desease";
            SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
            adpt.Fill(this.dB15DataSet.Desease);
            conn.Close();

        }

        private void Insert_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Desease_id_box.Text != "Desease id" && Desease_begin_box.Text != "Desease begin" && Desease_end_box.Text != "Desease end" && Desease_payment_box.Text != "Desease payment")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                string query = "";
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    query = "insert into Desease (Worker_id, Desease_id, Desease_begin, Desease_end, Desease_payment) values (" + Worker_id_box.Text + ", " + Desease_id_box.Text + ", '" + Desease_begin_box.Text + "', '" + Desease_end_box.Text + "', " + Desease_payment_box.Text + ")";
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Desease);
                    query = "select * from Desease";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Desease);
    //                this.deseaseTableAdapter.Fill(this.dB15DataSet.Desease);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Desease_id_box.Text = "Desease id";
                Desease_begin_box.Text = "Desease begin";
                Desease_end_box.Text = "Desease end";
                Desease_payment_box.Text = "Desease payment";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Update_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Desease_id_box.Text != "Desease id" && Desease_begin_box.Text != "Desease begin" && Desease_end_box.Text != "Desease end" && Desease_payment_box.Text != "Desease payment")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string row = Convert.ToString(dataGridView_desease.CurrentRow);
                    string row_str = "";
                    char[] arr = row.ToCharArray();
                    foreach (char h in arr)
                    {
                        if (char.IsDigit(h) == true)
                        {
                            row_str += h;
                        }
                    }
                    int row_num = Convert.ToInt32(row_str);
                    string query = "";
                    query = "update Desease set Worker_id=" + Worker_id_box.Text + ", Desease_id=" + Desease_id_box.Text + ", Desease_begin='" + Desease_begin_box.Text + "', Desease_end='" + Desease_end_box.Text + "', Desease_payment=" + Desease_payment_box.Text + " where Worker_id=" + dataGridView_desease.Rows[row_num].Cells[0].Value;
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Desease);
                    query = "select * from Desease";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Desease);
                   // this.deseaseTableAdapter.Fill(this.dB15DataSet.Desease);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Desease_id_box.Text = "Desease id";
                Desease_begin_box.Text = "Desease begin";
                Desease_end_box.Text = "Desease end";
                Desease_payment_box.Text = "Desease payment";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            try
            {
                conn.Open();
                string row = Convert.ToString(dataGridView_desease.CurrentRow);
                string row_str = "";
                char[] arr = row.ToCharArray();
                foreach (char h in arr)
                {
                    if (char.IsDigit(h) == true)
                    {
                        row_str += h;
                    }
                }
                int row_num = Convert.ToInt32(row_str);
                string query = "delete from Desease where Worker_id=" + dataGridView_desease.Rows[row_num].Cells[0].Value;
                SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                adpt.Fill(this.dB15DataSet.Desease);
                query = "select * from Desease";
                SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                adpt2.Fill(this.dB15DataSet.Desease);
             //   this.deseaseTableAdapter.Fill(this.dB15DataSet.Desease);
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
