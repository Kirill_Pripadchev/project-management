﻿namespace DB15_client
{
    partial class Worker_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_worker = new System.Windows.Forms.DataGridView();
            this.workeridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerbirthdayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workersexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerexperienceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.workerTableAdapter = new DB15_client.DB15DataSetTableAdapters.WorkerTableAdapter();
            this.Insert_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Delete_button = new System.Windows.Forms.Button();
            this.Worker_id_box = new System.Windows.Forms.TextBox();
            this.Worker_name_box = new System.Windows.Forms.TextBox();
            this.Worker_birthday_box = new System.Windows.Forms.TextBox();
            this.Worker_sex_box = new System.Windows.Forms.TextBox();
            this.Worker_experience_box = new System.Windows.Forms.TextBox();
            this.AddWorkerPlace_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_worker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_worker
            // 
            this.dataGridView_worker.AutoGenerateColumns = false;
            this.dataGridView_worker.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_worker.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workeridDataGridViewTextBoxColumn,
            this.workernameDataGridViewTextBoxColumn,
            this.workerbirthdayDataGridViewTextBoxColumn,
            this.workersexDataGridViewTextBoxColumn,
            this.workerexperienceDataGridViewTextBoxColumn});
            this.dataGridView_worker.DataSource = this.workerBindingSource;
            this.dataGridView_worker.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_worker.Name = "dataGridView_worker";
            this.dataGridView_worker.ReadOnly = true;
            this.dataGridView_worker.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_worker.Size = new System.Drawing.Size(567, 371);
            this.dataGridView_worker.TabIndex = 0;
            // 
            // workeridDataGridViewTextBoxColumn
            // 
            this.workeridDataGridViewTextBoxColumn.DataPropertyName = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.HeaderText = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.Name = "workeridDataGridViewTextBoxColumn";
            this.workeridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // workernameDataGridViewTextBoxColumn
            // 
            this.workernameDataGridViewTextBoxColumn.DataPropertyName = "Worker_name";
            this.workernameDataGridViewTextBoxColumn.HeaderText = "Worker_name";
            this.workernameDataGridViewTextBoxColumn.Name = "workernameDataGridViewTextBoxColumn";
            this.workernameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // workerbirthdayDataGridViewTextBoxColumn
            // 
            this.workerbirthdayDataGridViewTextBoxColumn.DataPropertyName = "Worker_birthday";
            this.workerbirthdayDataGridViewTextBoxColumn.HeaderText = "Worker_birthday";
            this.workerbirthdayDataGridViewTextBoxColumn.Name = "workerbirthdayDataGridViewTextBoxColumn";
            this.workerbirthdayDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // workersexDataGridViewTextBoxColumn
            // 
            this.workersexDataGridViewTextBoxColumn.DataPropertyName = "Worker_sex";
            this.workersexDataGridViewTextBoxColumn.HeaderText = "Worker_sex";
            this.workersexDataGridViewTextBoxColumn.Name = "workersexDataGridViewTextBoxColumn";
            this.workersexDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // workerexperienceDataGridViewTextBoxColumn
            // 
            this.workerexperienceDataGridViewTextBoxColumn.DataPropertyName = "Worker_experience";
            this.workerexperienceDataGridViewTextBoxColumn.HeaderText = "Worker_experience";
            this.workerexperienceDataGridViewTextBoxColumn.Name = "workerexperienceDataGridViewTextBoxColumn";
            this.workerexperienceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // workerBindingSource
            // 
            this.workerBindingSource.DataMember = "Worker";
            this.workerBindingSource.DataSource = this.dB15DataSet;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // workerTableAdapter
            // 
            this.workerTableAdapter.ClearBeforeFill = true;
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(573, 142);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 1;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(573, 198);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 2;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(573, 254);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 3;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Worker_id_box
            // 
            this.Worker_id_box.Location = new System.Drawing.Point(573, 12);
            this.Worker_id_box.Name = "Worker_id_box";
            this.Worker_id_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_id_box.TabIndex = 4;
            this.Worker_id_box.Text = "Worker id";
            // 
            // Worker_name_box
            // 
            this.Worker_name_box.Location = new System.Drawing.Point(573, 38);
            this.Worker_name_box.Name = "Worker_name_box";
            this.Worker_name_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_name_box.TabIndex = 5;
            this.Worker_name_box.Text = "Worker name";
            // 
            // Worker_birthday_box
            // 
            this.Worker_birthday_box.Location = new System.Drawing.Point(573, 64);
            this.Worker_birthday_box.Name = "Worker_birthday_box";
            this.Worker_birthday_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_birthday_box.TabIndex = 6;
            this.Worker_birthday_box.Text = "Worker birthday";
            // 
            // Worker_sex_box
            // 
            this.Worker_sex_box.Location = new System.Drawing.Point(573, 90);
            this.Worker_sex_box.Name = "Worker_sex_box";
            this.Worker_sex_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_sex_box.TabIndex = 7;
            this.Worker_sex_box.Text = "Worker sex";
            // 
            // Worker_experience_box
            // 
            this.Worker_experience_box.Location = new System.Drawing.Point(573, 116);
            this.Worker_experience_box.Name = "Worker_experience_box";
            this.Worker_experience_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_experience_box.TabIndex = 8;
            this.Worker_experience_box.Text = "Worker experience";
            // 
            // AddWorkerPlace_button
            // 
            this.AddWorkerPlace_button.Location = new System.Drawing.Point(573, 310);
            this.AddWorkerPlace_button.Name = "AddWorkerPlace_button";
            this.AddWorkerPlace_button.Size = new System.Drawing.Size(140, 50);
            this.AddWorkerPlace_button.TabIndex = 9;
            this.AddWorkerPlace_button.Text = "Добавить сотрудника и договор";
            this.AddWorkerPlace_button.UseVisualStyleBackColor = true;
            this.AddWorkerPlace_button.Click += new System.EventHandler(this.AddWorkerPlace_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(720, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(720, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(720, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "label3";
            this.label3.Visible = false;
            // 
            // Worker_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 371);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddWorkerPlace_button);
            this.Controls.Add(this.Worker_experience_box);
            this.Controls.Add(this.Worker_sex_box);
            this.Controls.Add(this.Worker_birthday_box);
            this.Controls.Add(this.Worker_name_box);
            this.Controls.Add(this.Worker_id_box);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.dataGridView_worker);
            this.Name = "Worker_form";
            this.Text = "Сотрудник";
            this.Load += new System.EventHandler(this.Worker_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_worker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource workerBindingSource;
        private DB15DataSetTableAdapters.WorkerTableAdapter workerTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn workeridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workernameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerbirthdayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workersexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerexperienceDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button Insert_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Delete_button;
        private System.Windows.Forms.TextBox Worker_id_box;
        private System.Windows.Forms.TextBox Worker_name_box;
        private System.Windows.Forms.TextBox Worker_birthday_box;
        private System.Windows.Forms.TextBox Worker_sex_box;
        private System.Windows.Forms.TextBox Worker_experience_box;
        private System.Windows.Forms.Button AddWorkerPlace_button;
        public System.Windows.Forms.DataGridView dataGridView_worker;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
    }
}