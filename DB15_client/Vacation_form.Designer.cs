﻿namespace DB15_client
{
    partial class Vacation_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_vacation = new System.Windows.Forms.DataGridView();
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.vacationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vacationTableAdapter = new DB15_client.DB15DataSetTableAdapters.VacationTableAdapter();
            this.workeridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vacationidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vacationbeginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vacationendDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vacationpaymentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Worker_id_box = new System.Windows.Forms.TextBox();
            this.Vacation_id_box = new System.Windows.Forms.TextBox();
            this.Vacation_begin_box = new System.Windows.Forms.TextBox();
            this.Vacation_end_box = new System.Windows.Forms.TextBox();
            this.Vacation_payment_box = new System.Windows.Forms.TextBox();
            this.Insert_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Delete_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_vacation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vacationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_vacation
            // 
            this.dataGridView_vacation.AutoGenerateColumns = false;
            this.dataGridView_vacation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_vacation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workeridDataGridViewTextBoxColumn,
            this.vacationidDataGridViewTextBoxColumn,
            this.vacationbeginDataGridViewTextBoxColumn,
            this.vacationendDataGridViewTextBoxColumn,
            this.vacationpaymentDataGridViewTextBoxColumn});
            this.dataGridView_vacation.DataSource = this.vacationBindingSource;
            this.dataGridView_vacation.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_vacation.Name = "dataGridView_vacation";
            this.dataGridView_vacation.ReadOnly = true;
            this.dataGridView_vacation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_vacation.Size = new System.Drawing.Size(569, 317);
            this.dataGridView_vacation.TabIndex = 0;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vacationBindingSource
            // 
            this.vacationBindingSource.DataMember = "Vacation";
            this.vacationBindingSource.DataSource = this.dB15DataSet;
            // 
            // vacationTableAdapter
            // 
            this.vacationTableAdapter.ClearBeforeFill = true;
            // 
            // workeridDataGridViewTextBoxColumn
            // 
            this.workeridDataGridViewTextBoxColumn.DataPropertyName = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.HeaderText = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.Name = "workeridDataGridViewTextBoxColumn";
            this.workeridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vacationidDataGridViewTextBoxColumn
            // 
            this.vacationidDataGridViewTextBoxColumn.DataPropertyName = "Vacation_id";
            this.vacationidDataGridViewTextBoxColumn.HeaderText = "Vacation_id";
            this.vacationidDataGridViewTextBoxColumn.Name = "vacationidDataGridViewTextBoxColumn";
            this.vacationidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vacationbeginDataGridViewTextBoxColumn
            // 
            this.vacationbeginDataGridViewTextBoxColumn.DataPropertyName = "Vacation_begin";
            this.vacationbeginDataGridViewTextBoxColumn.HeaderText = "Vacation_begin";
            this.vacationbeginDataGridViewTextBoxColumn.Name = "vacationbeginDataGridViewTextBoxColumn";
            this.vacationbeginDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vacationendDataGridViewTextBoxColumn
            // 
            this.vacationendDataGridViewTextBoxColumn.DataPropertyName = "Vacation_end";
            this.vacationendDataGridViewTextBoxColumn.HeaderText = "Vacation_end";
            this.vacationendDataGridViewTextBoxColumn.Name = "vacationendDataGridViewTextBoxColumn";
            this.vacationendDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vacationpaymentDataGridViewTextBoxColumn
            // 
            this.vacationpaymentDataGridViewTextBoxColumn.DataPropertyName = "Vacation_payment";
            this.vacationpaymentDataGridViewTextBoxColumn.HeaderText = "Vacation_payment";
            this.vacationpaymentDataGridViewTextBoxColumn.Name = "vacationpaymentDataGridViewTextBoxColumn";
            this.vacationpaymentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Worker_id_box
            // 
            this.Worker_id_box.Location = new System.Drawing.Point(575, 12);
            this.Worker_id_box.Name = "Worker_id_box";
            this.Worker_id_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_id_box.TabIndex = 1;
            this.Worker_id_box.Text = "Worker id";
            // 
            // Vacation_id_box
            // 
            this.Vacation_id_box.Location = new System.Drawing.Point(575, 38);
            this.Vacation_id_box.Name = "Vacation_id_box";
            this.Vacation_id_box.Size = new System.Drawing.Size(140, 20);
            this.Vacation_id_box.TabIndex = 2;
            this.Vacation_id_box.Text = "Vacation id";
            // 
            // Vacation_begin_box
            // 
            this.Vacation_begin_box.Location = new System.Drawing.Point(575, 64);
            this.Vacation_begin_box.Name = "Vacation_begin_box";
            this.Vacation_begin_box.Size = new System.Drawing.Size(140, 20);
            this.Vacation_begin_box.TabIndex = 3;
            this.Vacation_begin_box.Text = "Vacation begin";
            // 
            // Vacation_end_box
            // 
            this.Vacation_end_box.Location = new System.Drawing.Point(575, 90);
            this.Vacation_end_box.Name = "Vacation_end_box";
            this.Vacation_end_box.Size = new System.Drawing.Size(140, 20);
            this.Vacation_end_box.TabIndex = 4;
            this.Vacation_end_box.Text = "Vacation end";
            // 
            // Vacation_payment_box
            // 
            this.Vacation_payment_box.Location = new System.Drawing.Point(575, 116);
            this.Vacation_payment_box.Name = "Vacation_payment_box";
            this.Vacation_payment_box.Size = new System.Drawing.Size(140, 20);
            this.Vacation_payment_box.TabIndex = 5;
            this.Vacation_payment_box.Text = "Vacation payment";
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(575, 142);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 6;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(575, 198);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 7;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(575, 254);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 8;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Vacation_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 318);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.Vacation_payment_box);
            this.Controls.Add(this.Vacation_end_box);
            this.Controls.Add(this.Vacation_begin_box);
            this.Controls.Add(this.Vacation_id_box);
            this.Controls.Add(this.Worker_id_box);
            this.Controls.Add(this.dataGridView_vacation);
            this.Name = "Vacation_form";
            this.Text = "Отпуск";
            this.Load += new System.EventHandler(this.Vacation_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_vacation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vacationBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_vacation;
        private DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource vacationBindingSource;
        private DB15DataSetTableAdapters.VacationTableAdapter vacationTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn workeridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vacationidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vacationbeginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vacationendDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vacationpaymentDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox Worker_id_box;
        private System.Windows.Forms.TextBox Vacation_id_box;
        private System.Windows.Forms.TextBox Vacation_begin_box;
        private System.Windows.Forms.TextBox Vacation_end_box;
        private System.Windows.Forms.TextBox Vacation_payment_box;
        private System.Windows.Forms.Button Insert_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Delete_button;
    }
}