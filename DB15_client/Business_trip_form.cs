﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class Business_trip_form : Form
    {
        public Business_trip_form()
        {
            InitializeComponent();
        }

        private void Business_trip_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dB15DataSet.Business_trip". При необходимости она может быть перемещена или удалена.
            //this.business_tripTableAdapter.Fill(this.dB15DataSet.Business_trip);
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            conn.Open();
            string query = "select * from Business_trip";
            SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
            adpt.Fill(this.dB15DataSet.Business_trip);
            conn.Close();

        }

        private void Insert_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Business_trip_id_box.Text != "Business trip id" && Business_trip_begin_box.Text != "Business trip begin" && Business_trip_end_box.Text != "Business trip end" && Business_trip_purpose_box.Text != "Business trip purpose" && Business_trip_point_box.Text != "Business trip point" && Business_trip_payment_box.Text != "Business trip payment")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                string query = "";
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    query = "insert into Business_trip (Worker_id, Business_trip_id, Business_trip_begin, Business_trip_end, Business_trip_purpose, Business_trip_point, Business_trip_payment) values (" + Worker_id_box.Text + ", " + Business_trip_id_box.Text + ", '" + Business_trip_begin_box.Text + "', '" + Business_trip_end_box.Text + "', '" + Business_trip_purpose_box.Text + "', '" + Business_trip_point_box.Text + "', " + Business_trip_payment_box.Text + ")";
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Business_trip);
                    query = "select * from Business_trip";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Business_trip);
                    //this.business_tripTableAdapter.Fill(this.dB15DataSet.Business_trip);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Business_trip_id_box.Text = "Business trip id";
                Business_trip_begin_box.Text = "Business trip begin";
                Business_trip_end_box.Text = "Business trip end";
                Business_trip_purpose_box.Text = "Business trip purpose";
                Business_trip_point_box.Text = "Business trip point";
                Business_trip_payment_box.Text = "Business trip payment";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Update_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Business_trip_id_box.Text != "Business trip id" && Business_trip_begin_box.Text != "Business trip begin" && Business_trip_end_box.Text != "Business trip end" && Business_trip_purpose_box.Text != "Business trip purpose" && Business_trip_point_box.Text != "Business trip point" && Business_trip_payment_box.Text != "Business trip payment")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string row = Convert.ToString(dataGridView_business_trip.CurrentRow);
                    string row_str = "";
                    char[] arr = row.ToCharArray();
                    foreach (char h in arr)
                    {
                        if (char.IsDigit(h) == true)
                        {
                            row_str += h;
                        }
                    }
                    int row_num = Convert.ToInt32(row_str);
                    string query = "";
                    query = "update Business_trip set Worker_id=" + Worker_id_box.Text + ", Business_trip_id=" + Business_trip_id_box.Text + ", Business_trip_begin='" + Business_trip_begin_box.Text + "', Business_trip_end='" + Business_trip_end_box.Text + "', Business_trip_purpose='" + Business_trip_purpose_box.Text + "', Business_trip_point='" + Business_trip_point_box.Text + "', Business_trip_payment=" + Business_trip_payment_box.Text + " where Worker_id=" + dataGridView_business_trip.Rows[row_num].Cells[0].Value;
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Business_trip);
                    query = "select * from Business_trip";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Business_trip);
                    //this.business_tripTableAdapter.Fill(this.dB15DataSet.Business_trip);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Business_trip_id_box.Text = "Business trip id";
                Business_trip_begin_box.Text = "Business trip begin";
                Business_trip_end_box.Text = "Business trip end";
                Business_trip_purpose_box.Text = "Business trip purpose";
                Business_trip_point_box.Text = "Business trip point";
                Business_trip_payment_box.Text = "Business trip payment";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            try
            {
                conn.Open();
                string row = Convert.ToString(dataGridView_business_trip.CurrentRow);
                string row_str = "";
                char[] arr = row.ToCharArray();
                foreach (char h in arr)
                {
                    if (char.IsDigit(h) == true)
                    {
                        row_str += h;
                    }
                }
                int row_num = Convert.ToInt32(row_str);
                string query = "delete from Business_trip where Worker_id=" + dataGridView_business_trip.Rows[row_num].Cells[0].Value;
                SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                adpt.Fill(this.dB15DataSet.Business_trip);
                query = "select * from Business_trip";
                SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                adpt2.Fill(this.dB15DataSet.Business_trip);
                //this.business_tripTableAdapter.Fill(this.dB15DataSet.Business_trip);
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
