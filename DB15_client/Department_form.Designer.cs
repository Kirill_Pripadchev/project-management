﻿namespace DB15_client
{
    partial class Department_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_department = new System.Windows.Forms.DataGridView();
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.departmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.departmentTableAdapter = new DB15_client.DB15DataSetTableAdapters.DepartmentTableAdapter();
            this.departmentidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Department_id_box = new System.Windows.Forms.TextBox();
            this.Department_name_box = new System.Windows.Forms.TextBox();
            this.Insert_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Delete_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_department)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_department
            // 
            this.dataGridView_department.AutoGenerateColumns = false;
            this.dataGridView_department.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_department.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.departmentidDataGridViewTextBoxColumn,
            this.departmentnameDataGridViewTextBoxColumn});
            this.dataGridView_department.DataSource = this.departmentBindingSource;
            this.dataGridView_department.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_department.Name = "dataGridView_department";
            this.dataGridView_department.ReadOnly = true;
            this.dataGridView_department.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_department.Size = new System.Drawing.Size(273, 238);
            this.dataGridView_department.TabIndex = 0;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // departmentBindingSource
            // 
            this.departmentBindingSource.DataMember = "Department";
            this.departmentBindingSource.DataSource = this.dB15DataSet;
            // 
            // departmentTableAdapter
            // 
            this.departmentTableAdapter.ClearBeforeFill = true;
            // 
            // departmentidDataGridViewTextBoxColumn
            // 
            this.departmentidDataGridViewTextBoxColumn.DataPropertyName = "Department_id";
            this.departmentidDataGridViewTextBoxColumn.HeaderText = "Department_id";
            this.departmentidDataGridViewTextBoxColumn.Name = "departmentidDataGridViewTextBoxColumn";
            this.departmentidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // departmentnameDataGridViewTextBoxColumn
            // 
            this.departmentnameDataGridViewTextBoxColumn.DataPropertyName = "Department_name";
            this.departmentnameDataGridViewTextBoxColumn.HeaderText = "Department_name";
            this.departmentnameDataGridViewTextBoxColumn.Name = "departmentnameDataGridViewTextBoxColumn";
            this.departmentnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Department_id_box
            // 
            this.Department_id_box.Location = new System.Drawing.Point(279, 12);
            this.Department_id_box.Name = "Department_id_box";
            this.Department_id_box.Size = new System.Drawing.Size(140, 20);
            this.Department_id_box.TabIndex = 1;
            this.Department_id_box.Text = "Department id";
            // 
            // Department_name_box
            // 
            this.Department_name_box.Location = new System.Drawing.Point(279, 38);
            this.Department_name_box.Name = "Department_name_box";
            this.Department_name_box.Size = new System.Drawing.Size(140, 20);
            this.Department_name_box.TabIndex = 2;
            this.Department_name_box.Text = "Department name";
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(279, 64);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 3;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(279, 120);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 4;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(279, 176);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 5;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Department_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 239);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.Department_name_box);
            this.Controls.Add(this.Department_id_box);
            this.Controls.Add(this.dataGridView_department);
            this.Name = "Department_form";
            this.Text = "Отдел";
            this.Load += new System.EventHandler(this.Department_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_department)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_department;
        private DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource departmentBindingSource;
        private DB15DataSetTableAdapters.DepartmentTableAdapter departmentTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox Department_id_box;
        private System.Windows.Forms.TextBox Department_name_box;
        private System.Windows.Forms.Button Insert_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Delete_button;
    }
}