﻿namespace DB15_client
{
    partial class AddWorkerPlace_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Worker_experience_box = new System.Windows.Forms.TextBox();
            this.Worker_sex_box = new System.Windows.Forms.TextBox();
            this.Worker_birthday_box = new System.Windows.Forms.TextBox();
            this.Worker_name_box = new System.Windows.Forms.TextBox();
            this.Worker_id_box = new System.Windows.Forms.TextBox();
            this.Contract_begin_box = new System.Windows.Forms.TextBox();
            this.Post_id_box = new System.Windows.Forms.TextBox();
            this.Contract_id_box = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dB15DataSet1 = new DB15_client.DB15DataSet();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // Worker_experience_box
            // 
            this.Worker_experience_box.Location = new System.Drawing.Point(12, 116);
            this.Worker_experience_box.Name = "Worker_experience_box";
            this.Worker_experience_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_experience_box.TabIndex = 13;
            this.Worker_experience_box.Text = "Worker experience";
            // 
            // Worker_sex_box
            // 
            this.Worker_sex_box.Location = new System.Drawing.Point(12, 90);
            this.Worker_sex_box.Name = "Worker_sex_box";
            this.Worker_sex_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_sex_box.TabIndex = 12;
            this.Worker_sex_box.Text = "Worker sex";
            // 
            // Worker_birthday_box
            // 
            this.Worker_birthday_box.Location = new System.Drawing.Point(12, 64);
            this.Worker_birthday_box.Name = "Worker_birthday_box";
            this.Worker_birthday_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_birthday_box.TabIndex = 11;
            this.Worker_birthday_box.Text = "Worker birthday";
            // 
            // Worker_name_box
            // 
            this.Worker_name_box.Location = new System.Drawing.Point(12, 38);
            this.Worker_name_box.Name = "Worker_name_box";
            this.Worker_name_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_name_box.TabIndex = 10;
            this.Worker_name_box.Text = "Worker name";
            // 
            // Worker_id_box
            // 
            this.Worker_id_box.Location = new System.Drawing.Point(12, 12);
            this.Worker_id_box.Name = "Worker_id_box";
            this.Worker_id_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_id_box.TabIndex = 9;
            this.Worker_id_box.Text = "Worker id";
            // 
            // Contract_begin_box
            // 
            this.Contract_begin_box.Location = new System.Drawing.Point(158, 64);
            this.Contract_begin_box.Name = "Contract_begin_box";
            this.Contract_begin_box.Size = new System.Drawing.Size(140, 20);
            this.Contract_begin_box.TabIndex = 16;
            this.Contract_begin_box.Text = "Contract begin";
            // 
            // Post_id_box
            // 
            this.Post_id_box.Location = new System.Drawing.Point(158, 38);
            this.Post_id_box.Name = "Post_id_box";
            this.Post_id_box.Size = new System.Drawing.Size(140, 20);
            this.Post_id_box.TabIndex = 15;
            this.Post_id_box.Text = "Post id";
            // 
            // Contract_id_box
            // 
            this.Contract_id_box.Location = new System.Drawing.Point(158, 12);
            this.Contract_id_box.Name = "Contract_id_box";
            this.Contract_id_box.Size = new System.Drawing.Size(140, 20);
            this.Contract_id_box.TabIndex = 14;
            this.Contract_id_box.Text = "Contract id";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(286, 50);
            this.button1.TabIndex = 17;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dB15DataSet1
            // 
            this.dB15DataSet1.DataSetName = "DB15DataSet";
            this.dB15DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AddWorkerPlace_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 203);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Contract_begin_box);
            this.Controls.Add(this.Post_id_box);
            this.Controls.Add(this.Contract_id_box);
            this.Controls.Add(this.Worker_experience_box);
            this.Controls.Add(this.Worker_sex_box);
            this.Controls.Add(this.Worker_birthday_box);
            this.Controls.Add(this.Worker_name_box);
            this.Controls.Add(this.Worker_id_box);
            this.Name = "AddWorkerPlace_form";
            this.Text = "AddWorkerPlace_form";
            this.Load += new System.EventHandler(this.AddWorkerPlace_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Worker_experience_box;
        private System.Windows.Forms.TextBox Worker_sex_box;
        private System.Windows.Forms.TextBox Worker_birthday_box;
        private System.Windows.Forms.TextBox Worker_name_box;
        private System.Windows.Forms.TextBox Worker_id_box;
        private System.Windows.Forms.TextBox Contract_begin_box;
        private System.Windows.Forms.TextBox Post_id_box;
        private System.Windows.Forms.TextBox Contract_id_box;
        private System.Windows.Forms.Button button1;
        private DB15DataSet dB15DataSet1;
    }
}