﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class Department_form : Form
    {
        public Department_form()
        {
            InitializeComponent();
        }

        private void Department_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dB15DataSet.Department". При необходимости она может быть перемещена или удалена.
            //this.departmentTableAdapter.Fill(this.dB15DataSet.Department);
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            conn.Open();
            string query = "select * from Department";
            SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
            adpt.Fill(this.dB15DataSet.Department);
            conn.Close();

        }

        private void Insert_button_Click(object sender, EventArgs e)
        {
            if (Department_id_box.Text != "Department id" && Department_name_box.Text != "Department name")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string query = "insert into Department (Department_id, Department_name) values (" + Department_id_box.Text + ", '" + Department_name_box.Text + "')";
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Department);
                    query = "select * from Department";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Department);
                    //this.departmentTableAdapter.Fill(this.dB15DataSet.Department);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Department_id_box.Text = "Department id";
                Department_name_box.Text = "Department name";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Update_button_Click(object sender, EventArgs e)
        {
            if (Department_id_box.Text != "Department id" && Department_name_box.Text != "Department name")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string row = Convert.ToString(dataGridView_department.CurrentRow);
                    string row_str = "";
                    char[] arr = row.ToCharArray();
                    foreach (char h in arr)
                    {
                        if (char.IsDigit(h) == true)
                        {
                            row_str += h;
                        }
                    }
                    int row_num = Convert.ToInt32(row_str);
                    string query = "update Department set Department_id=" + Department_id_box.Text + ", Department_name='" + Department_name_box.Text + "' where Department_id=" + dataGridView_department.Rows[row_num].Cells[0].Value;
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Department);
                    query = "select * from Department";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Department);
                    //this.departmentTableAdapter.Fill(this.dB15DataSet.Department);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Department_id_box.Text = "Department id";
                Department_name_box.Text = "Department name";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            try
            {
                conn.Open();
                string row = Convert.ToString(dataGridView_department.CurrentRow);
                string row_str = "";
                char[] arr = row.ToCharArray();
                foreach (char h in arr)
                {
                    if (char.IsDigit(h) == true)
                    {
                        row_str += h;
                    }
                }
                int row_num = Convert.ToInt32(row_str);
                string query = "delete from Department where Department_id=" + dataGridView_department.Rows[row_num].Cells[0].Value;
                SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                adpt.Fill(this.dB15DataSet.Department);
                query = "select * from Department";
                SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                adpt2.Fill(this.dB15DataSet.Department);
               // this.departmentTableAdapter.Fill(this.dB15DataSet.Department);
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
