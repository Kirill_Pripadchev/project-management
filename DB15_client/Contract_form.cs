﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class Contract_form : Form
    {
        public Contract_form()
        {
            InitializeComponent();
        }

        private void Contract_form_Load(object sender, EventArgs e)
        {
            Contract_end_box.Enabled = false;
            Contract_end_checkBox.Checked = false;
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            conn.Open();
            string query = "select * from Contract";
            SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
            adpt.Fill(this.dB15DataSet.Contract);
            conn.Close();
        }

        private void Insert_button_Click(object sender, EventArgs e)
        {
            if (Contract_id_box.Text != "Contract id" && Post_id_box.Text != "Post id" && Worker_id_box.Text != "Worker id" && Contract_begin_box.Text != "Contract begin")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                string query = "";
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    if (Contract_end_checkBox.Checked == true)
                    {
                        query = "insert into Contract (Contract_id, Post_id, Worker_id, Contract_begin, Contract_end) values (" + Contract_id_box.Text + ", " + Post_id_box.Text + ", " + Worker_id_box.Text + ", '" + Contract_begin_box.Text + "', '" + Contract_end_box.Text + "')";
                    }
                    else 
                    {
                        query = "insert into Contract (Contract_id, Post_id, Worker_id, Contract_begin) values (" + Contract_id_box.Text + ", " + Post_id_box.Text + ", " + Worker_id_box.Text + ", '" + Contract_begin_box.Text + "')";
                    }
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Contract);
                    query = "select * from Contract";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Contract);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Contract_id_box.Text = "Contract id";
                Post_id_box.Text = "Post id";
                Worker_id_box.Text = "Worker id";
                Contract_begin_box.Text = "Contract begin";
                Contract_end_box.Text = "Contract end";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Contract_end_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Contract_end_checkBox.Checked == true) 
            {
                Contract_end_box.Enabled = true;
            }
            if (Contract_end_checkBox.Checked == false) 
            {
                Contract_end_box.Enabled = false;
            }
        }

        private void Update_button_Click(object sender, EventArgs e)
        {
            if (Contract_id_box.Text != "Contract id" && Post_id_box.Text != "Post id" && Worker_id_box.Text != "Worker id" && Contract_begin_box.Text != "Contract begin")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string row = Convert.ToString(dataGridView_contract.CurrentRow);
                    string row_str = "";
                    char[] arr = row.ToCharArray();
                    foreach (char h in arr)
                    {
                        if (char.IsDigit(h) == true)
                        {
                            row_str += h;
                        }
                    }
                    int row_num = Convert.ToInt32(row_str);
                    string query = "";
                    if (Contract_end_checkBox.Checked == true)
                    {
                        query = "update Contract set Contract_id=" + Contract_id_box.Text + ", Post_id=" + Post_id_box.Text + ", Worker_id=" + Worker_id_box.Text + ", Contract_begin='" + Contract_begin_box.Text + "', Contract_end='" + Contract_end_box.Text + "' where Contract_id=" + dataGridView_contract.Rows[row_num].Cells[0].Value;
                    }
                    else 
                    {
                        query = "update Contract set Contract_id=" + Contract_id_box.Text + ", Post_id=" + Post_id_box.Text + ", Worker_id=" + Worker_id_box.Text + ", Contract_begin='" + Contract_begin_box.Text + "' where Contract_id=" + dataGridView_contract.Rows[row_num].Cells[0].Value;
                    }
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Contract);
                    query = "select * from Contract";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Contract);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Contract_id_box.Text = "Contract id";
                Post_id_box.Text = "Post id";
                Worker_id_box.Text = "Worker id";
                Contract_begin_box.Text = "Contract begin";
                Contract_end_box.Text = "Contract end";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            try
            {
                conn.Open();
                string row = Convert.ToString(dataGridView_contract.CurrentRow);
                string row_str = "";
                char[] arr = row.ToCharArray();
                foreach (char h in arr)
                {
                    if (char.IsDigit(h) == true)
                    {
                        row_str += h;
                    }
                }
                int row_num = Convert.ToInt32(row_str);
                string query = "delete from Contract where Contract_id=" + dataGridView_contract.Rows[row_num].Cells[0].Value;
                SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                adpt.Fill(this.dB15DataSet.Contract);
                query = "select * from Contract";
                SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                adpt2.Fill(this.dB15DataSet.Contract);
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
