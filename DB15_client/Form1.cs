﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB15_client
{
    public partial class Form1 : Form
    {
        Business_trip_form bsn_form = new Business_trip_form();
        Characteristic_form chr_form = new Characteristic_form();
        Contract_form ctr_form = new Contract_form();
        Department_form dpt_form = new Department_form();
        Desease_form ds_form = new Desease_form();
        Post_form pst_form = new Post_form();
        Settings_form St = new Settings_form();
        Vacation_form vct_form = new Vacation_form();
        Worker_form wrk_fm = new Worker_form();
        AddWorkerPlace_form awp_fm = new AddWorkerPlace_form();
        Report_form rpt_fm = new Report_form();
        public Form1()
        {
            InitializeComponent();
        }

        private void Worker_button_Click(object sender, EventArgs e)
        {
            wrk_fm.ShowDialog();
        }

        private void Contract_button_Click(object sender, EventArgs e)
        {
            ctr_form.ShowDialog();
        }

        private void Department_button_Click(object sender, EventArgs e)
        {
            dpt_form.ShowDialog();
        }

        private void Post_button_Click(object sender, EventArgs e)
        {
            pst_form.ShowDialog();
        }

        private void Vacation_button_Click(object sender, EventArgs e)
        {
            vct_form.ShowDialog();
        }

        private void Desease_button_Click(object sender, EventArgs e)
        {
            ds_form.ShowDialog();
        }

        private void Business_trip_button_Click(object sender, EventArgs e)
        {
            bsn_form.ShowDialog();
        }

        private void Characteristic_button_Click(object sender, EventArgs e)
        {
            chr_form.ShowDialog();
        }

        private void Path_button_Click(object sender, EventArgs e)
        {
            St.ShowDialog();
        }

        private void Main_form_Load(object sender, EventArgs e)
        {
            label1.Text = "MICROSOFT-PC";
            label2.Text = "DB15";
            label3.Text = "True";
            bsn_form.Owner = this;
            AddOwnedForm(bsn_form);
            chr_form.Owner = this;
            AddOwnedForm(chr_form);
            ctr_form.Owner = this;
            AddOwnedForm(ctr_form);
            dpt_form.Owner = this;
            AddOwnedForm(dpt_form);
            ds_form.Owner = this;
            AddOwnedForm(ds_form);
            pst_form.Owner = this;
            AddOwnedForm(pst_form);
            St.Owner = this;
            AddOwnedForm(St);
            vct_form.Owner = this;
            AddOwnedForm(vct_form);
            wrk_fm.Owner = this;
            AddOwnedForm(wrk_fm);
            rpt_fm.Owner = this;
            AddOwnedForm(rpt_fm);
        }

        private void Report_button_Click(object sender, EventArgs e)
        {
            rpt_fm.ShowDialog();
        }
    }
}
