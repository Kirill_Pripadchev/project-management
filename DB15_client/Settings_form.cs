﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

namespace DB15_client
{
    public partial class Settings_form : Form
    {
        public Settings_form()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            main.label1.Text = Data_source_box.Text;
            main.label2.Text = Initial_catalog_box.Text;
            main.label3.Text = Integrated_security_box.Text;
        }

        private void Settings_form_Load(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            Data_source_box.Text = main.label1.Text;
            Initial_catalog_box.Text = main.label2.Text;
            Integrated_security_box.Text = main.label3.Text;
        }
    }
}
