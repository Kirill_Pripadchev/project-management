﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class Vacation_form : Form
    {
        public Vacation_form()
        {
            InitializeComponent();
        }

        private void Vacation_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dB15DataSet.Vacation". При необходимости она может быть перемещена или удалена.
            //this.vacationTableAdapter.Fill(this.dB15DataSet.Vacation);
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            conn.Open();
            string query = "select * from Vacation";
            SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
            adpt.Fill(this.dB15DataSet.Vacation);
            conn.Close();

        }

        private void Insert_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Vacation_id_box.Text != "Vacation id" && Vacation_begin_box.Text != "Vacation begin" && Vacation_end_box.Text != "Vacation end" && Vacation_payment_box.Text != "Vacation payment")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                string query = "";
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    query = "insert into Vacation (Worker_id, Vacation_id, Vacation_begin, Vacation_end, Vacation_payment) values (" + Worker_id_box.Text + ", " + Vacation_id_box.Text + ", '" + Vacation_begin_box.Text + "', '" + Vacation_end_box.Text + "', " + Vacation_payment_box.Text + ")";
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Worker);
                    query = "select * from Vacation";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Vacation);
                    //this.vacationTableAdapter.Fill(this.dB15DataSet.Vacation);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Vacation_id_box.Text = "Vacation id";
                Vacation_begin_box.Text = "Vacation begin";
                Vacation_end_box.Text = "Vacation end";
                Vacation_payment_box.Text = "Vacation payment";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Update_button_Click(object sender, EventArgs e)
        {
            if (Worker_id_box.Text != "Worker id" && Vacation_id_box.Text != "Vacation id" && Vacation_begin_box.Text != "Vacation begin" && Vacation_end_box.Text != "Vacation end" && Vacation_payment_box.Text != "Vacation payment")
            {
                Form1 main = this.Owner as Form1;
                string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string row = Convert.ToString(dataGridView_vacation.CurrentRow);
                    string row_str = "";
                    char[] arr = row.ToCharArray();
                    foreach (char h in arr)
                    {
                        if (char.IsDigit(h) == true)
                        {
                            row_str += h;
                        }
                    }
                    int row_num = Convert.ToInt32(row_str);
                    string query = "";
                    query = "update Vacation set Worker_id=" + Worker_id_box.Text + ", Vacation_id=" + Vacation_id_box.Text + ", Vacation_begin='" + Vacation_begin_box.Text + "', Vacation_end='" + Vacation_end_box.Text + "', Vacation_payment=" + Vacation_payment_box.Text + " where Worker_id=" + dataGridView_vacation.Rows[row_num].Cells[0].Value;
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(this.dB15DataSet.Vacation);
                    query = "select * from Vacation";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(this.dB15DataSet.Vacation);
                    //this.vacationTableAdapter.Fill(this.dB15DataSet.Vacation);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Vacation_id_box.Text = "Vacation id";
                Vacation_begin_box.Text = "Vacation begin";
                Vacation_end_box.Text = "Vacation end";
                Vacation_payment_box.Text = "Vacation payment";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_button_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            string connection_source = @"Data Source=" + main.label1.Text + ";Initial Catalog=" + main.label2.Text + ";Integrated Security=" + main.label3.Text;
            SqlConnection conn = new SqlConnection(connection_source);
            try
            {
                conn.Open();
                string row = Convert.ToString(dataGridView_vacation.CurrentRow);
                string row_str = "";
                char[] arr = row.ToCharArray();
                foreach (char h in arr)
                {
                    if (char.IsDigit(h) == true)
                    {
                        row_str += h;
                    }
                }
                int row_num = Convert.ToInt32(row_str);
                string query = "delete from Vacation where Worker_id=" + dataGridView_vacation.Rows[row_num].Cells[0].Value;
                SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                adpt.Fill(this.dB15DataSet.Worker);
                query = "select * from Vacation";
                SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                adpt2.Fill(this.dB15DataSet.Vacation);
                //this.vacationTableAdapter.Fill(this.dB15DataSet.Vacation);
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
