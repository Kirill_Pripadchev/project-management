﻿namespace DB15_client
{
    partial class Desease_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_desease = new System.Windows.Forms.DataGridView();
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.deseaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deseaseTableAdapter = new DB15_client.DB15DataSetTableAdapters.DeseaseTableAdapter();
            this.workeridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deseaseidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deseasebeginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deseaseendDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deseasepaymentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Worker_id_box = new System.Windows.Forms.TextBox();
            this.Desease_id_box = new System.Windows.Forms.TextBox();
            this.Desease_begin_box = new System.Windows.Forms.TextBox();
            this.Desease_end_box = new System.Windows.Forms.TextBox();
            this.Desease_payment_box = new System.Windows.Forms.TextBox();
            this.Delete_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Insert_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_desease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deseaseBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_desease
            // 
            this.dataGridView_desease.AutoGenerateColumns = false;
            this.dataGridView_desease.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_desease.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workeridDataGridViewTextBoxColumn,
            this.deseaseidDataGridViewTextBoxColumn,
            this.deseasebeginDataGridViewTextBoxColumn,
            this.deseaseendDataGridViewTextBoxColumn,
            this.deseasepaymentDataGridViewTextBoxColumn});
            this.dataGridView_desease.DataSource = this.deseaseBindingSource;
            this.dataGridView_desease.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_desease.Name = "dataGridView_desease";
            this.dataGridView_desease.ReadOnly = true;
            this.dataGridView_desease.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_desease.Size = new System.Drawing.Size(567, 313);
            this.dataGridView_desease.TabIndex = 0;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // deseaseBindingSource
            // 
            this.deseaseBindingSource.DataMember = "Desease";
            this.deseaseBindingSource.DataSource = this.dB15DataSet;
            // 
            // deseaseTableAdapter
            // 
            this.deseaseTableAdapter.ClearBeforeFill = true;
            // 
            // workeridDataGridViewTextBoxColumn
            // 
            this.workeridDataGridViewTextBoxColumn.DataPropertyName = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.HeaderText = "Worker_id";
            this.workeridDataGridViewTextBoxColumn.Name = "workeridDataGridViewTextBoxColumn";
            this.workeridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deseaseidDataGridViewTextBoxColumn
            // 
            this.deseaseidDataGridViewTextBoxColumn.DataPropertyName = "Desease_id";
            this.deseaseidDataGridViewTextBoxColumn.HeaderText = "Desease_id";
            this.deseaseidDataGridViewTextBoxColumn.Name = "deseaseidDataGridViewTextBoxColumn";
            this.deseaseidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deseasebeginDataGridViewTextBoxColumn
            // 
            this.deseasebeginDataGridViewTextBoxColumn.DataPropertyName = "Desease_begin";
            this.deseasebeginDataGridViewTextBoxColumn.HeaderText = "Desease_begin";
            this.deseasebeginDataGridViewTextBoxColumn.Name = "deseasebeginDataGridViewTextBoxColumn";
            this.deseasebeginDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deseaseendDataGridViewTextBoxColumn
            // 
            this.deseaseendDataGridViewTextBoxColumn.DataPropertyName = "Desease_end";
            this.deseaseendDataGridViewTextBoxColumn.HeaderText = "Desease_end";
            this.deseaseendDataGridViewTextBoxColumn.Name = "deseaseendDataGridViewTextBoxColumn";
            this.deseaseendDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deseasepaymentDataGridViewTextBoxColumn
            // 
            this.deseasepaymentDataGridViewTextBoxColumn.DataPropertyName = "Desease_payment";
            this.deseasepaymentDataGridViewTextBoxColumn.HeaderText = "Desease_payment";
            this.deseasepaymentDataGridViewTextBoxColumn.Name = "deseasepaymentDataGridViewTextBoxColumn";
            this.deseasepaymentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Worker_id_box
            // 
            this.Worker_id_box.Location = new System.Drawing.Point(573, 12);
            this.Worker_id_box.Name = "Worker_id_box";
            this.Worker_id_box.Size = new System.Drawing.Size(140, 20);
            this.Worker_id_box.TabIndex = 1;
            this.Worker_id_box.Text = "Worker id";
            // 
            // Desease_id_box
            // 
            this.Desease_id_box.Location = new System.Drawing.Point(573, 38);
            this.Desease_id_box.Name = "Desease_id_box";
            this.Desease_id_box.Size = new System.Drawing.Size(140, 20);
            this.Desease_id_box.TabIndex = 2;
            this.Desease_id_box.Text = "Desease id";
            // 
            // Desease_begin_box
            // 
            this.Desease_begin_box.Location = new System.Drawing.Point(573, 64);
            this.Desease_begin_box.Name = "Desease_begin_box";
            this.Desease_begin_box.Size = new System.Drawing.Size(140, 20);
            this.Desease_begin_box.TabIndex = 3;
            this.Desease_begin_box.Text = "Desease begin";
            // 
            // Desease_end_box
            // 
            this.Desease_end_box.Location = new System.Drawing.Point(573, 90);
            this.Desease_end_box.Name = "Desease_end_box";
            this.Desease_end_box.Size = new System.Drawing.Size(140, 20);
            this.Desease_end_box.TabIndex = 4;
            this.Desease_end_box.Text = "Desease end";
            // 
            // Desease_payment_box
            // 
            this.Desease_payment_box.Location = new System.Drawing.Point(573, 116);
            this.Desease_payment_box.Name = "Desease_payment_box";
            this.Desease_payment_box.Size = new System.Drawing.Size(140, 20);
            this.Desease_payment_box.TabIndex = 5;
            this.Desease_payment_box.Text = "Desease payment";
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(573, 254);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 11;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(573, 198);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 10;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(573, 142);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 9;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Desease_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 313);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.Desease_payment_box);
            this.Controls.Add(this.Desease_end_box);
            this.Controls.Add(this.Desease_begin_box);
            this.Controls.Add(this.Desease_id_box);
            this.Controls.Add(this.Worker_id_box);
            this.Controls.Add(this.dataGridView_desease);
            this.Name = "Desease_form";
            this.Text = "Больничный";
            this.Load += new System.EventHandler(this.Desease_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_desease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deseaseBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_desease;
        private DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource deseaseBindingSource;
        private DB15DataSetTableAdapters.DeseaseTableAdapter deseaseTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn workeridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deseaseidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deseasebeginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deseaseendDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deseasepaymentDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox Worker_id_box;
        private System.Windows.Forms.TextBox Desease_id_box;
        private System.Windows.Forms.TextBox Desease_begin_box;
        private System.Windows.Forms.TextBox Desease_end_box;
        private System.Windows.Forms.TextBox Desease_payment_box;
        private System.Windows.Forms.Button Delete_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Insert_button;
    }
}