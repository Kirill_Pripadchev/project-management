﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB15_client
{
    public partial class AddWorkerPlace_form : Form
    {
        public AddWorkerPlace_form()
        {
            InitializeComponent();
        }

        private void AddWorkerPlace_form_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Worker_form worker_main = this.Owner as Worker_form;
            if (Worker_id_box.Text != "Worker id" && Worker_name_box.Text != "Worker name" && Worker_birthday_box.Text != "Worker birthday" && Worker_sex_box.Text != "Worker sex" && Worker_experience_box.Text != "Worker experience" && Contract_id_box.Text != "Contract id" && Post_id_box.Text != "Post id" && Contract_begin_box.Text != "Contract begin")
            {
                string connection_source = @"Data Source=" + worker_main.label1.Text + ";Initial Catalog=" + worker_main.label2.Text + ";Integrated Security=" + worker_main.label3.Text;
                SqlConnection conn = new SqlConnection(connection_source);
                try
                {
                    conn.Open();
                    string query = "exec AddWorkerPlace "+Worker_id_box.Text+", '"+Worker_name_box.Text+"', '"+Worker_birthday_box.Text+"', '"+Worker_sex_box.Text+"', "+Worker_experience_box.Text+", "+Contract_id_box.Text+", "+Post_id_box.Text+", '"+Contract_begin_box.Text+"'";
                    SqlDataAdapter adpt = new SqlDataAdapter(query, conn);
                    adpt.Fill(worker_main.dB15DataSet.Worker);
                    query = "select * from Worker";
                    SqlDataAdapter adpt2 = new SqlDataAdapter(query, conn);
                    adpt2.Fill(worker_main.dB15DataSet.Worker);
                    //this.workerTableAdapter.Fill(this.dB15DataSet.Worker);
                    conn.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Worker_id_box.Text = "Worker id";
                Worker_name_box.Text = "Worker name";
                Worker_birthday_box.Text = "Worker birthday";
                Worker_sex_box.Text = "Worker sex";
                Worker_experience_box.Text = "Worker experience";
                Contract_id_box.Text = "Contract id";
                Post_id_box.Text = "Post id";
                Contract_begin_box.Text = "Contract begin";
            }
            else
            {
                MessageBox.Show("Введите корректные значения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
