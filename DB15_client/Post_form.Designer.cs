﻿namespace DB15_client
{
    partial class Post_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView_post = new System.Windows.Forms.DataGridView();
            this.postidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postsalaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postbossDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.postBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dB15DataSet = new DB15_client.DB15DataSet();
            this.postTableAdapter = new DB15_client.DB15DataSetTableAdapters.PostTableAdapter();
            this.Post_id_box = new System.Windows.Forms.TextBox();
            this.Department_id_box = new System.Windows.Forms.TextBox();
            this.Post_name_box = new System.Windows.Forms.TextBox();
            this.Post_salary_box = new System.Windows.Forms.TextBox();
            this.Post_boss_checkBox = new System.Windows.Forms.CheckBox();
            this.Insert_button = new System.Windows.Forms.Button();
            this.Update_button = new System.Windows.Forms.Button();
            this.Delete_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_post)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.postBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_post
            // 
            this.dataGridView_post.AutoGenerateColumns = false;
            this.dataGridView_post.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_post.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.postidDataGridViewTextBoxColumn,
            this.departmentidDataGridViewTextBoxColumn,
            this.postnameDataGridViewTextBoxColumn,
            this.postsalaryDataGridViewTextBoxColumn,
            this.postbossDataGridViewCheckBoxColumn});
            this.dataGridView_post.DataSource = this.postBindingSource;
            this.dataGridView_post.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_post.Name = "dataGridView_post";
            this.dataGridView_post.ReadOnly = true;
            this.dataGridView_post.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_post.Size = new System.Drawing.Size(566, 318);
            this.dataGridView_post.TabIndex = 0;
            // 
            // postidDataGridViewTextBoxColumn
            // 
            this.postidDataGridViewTextBoxColumn.DataPropertyName = "Post_id";
            this.postidDataGridViewTextBoxColumn.HeaderText = "Post_id";
            this.postidDataGridViewTextBoxColumn.Name = "postidDataGridViewTextBoxColumn";
            this.postidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // departmentidDataGridViewTextBoxColumn
            // 
            this.departmentidDataGridViewTextBoxColumn.DataPropertyName = "Department_id";
            this.departmentidDataGridViewTextBoxColumn.HeaderText = "Department_id";
            this.departmentidDataGridViewTextBoxColumn.Name = "departmentidDataGridViewTextBoxColumn";
            this.departmentidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // postnameDataGridViewTextBoxColumn
            // 
            this.postnameDataGridViewTextBoxColumn.DataPropertyName = "Post_name";
            this.postnameDataGridViewTextBoxColumn.HeaderText = "Post_name";
            this.postnameDataGridViewTextBoxColumn.Name = "postnameDataGridViewTextBoxColumn";
            this.postnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // postsalaryDataGridViewTextBoxColumn
            // 
            this.postsalaryDataGridViewTextBoxColumn.DataPropertyName = "Post_salary";
            this.postsalaryDataGridViewTextBoxColumn.HeaderText = "Post_salary";
            this.postsalaryDataGridViewTextBoxColumn.Name = "postsalaryDataGridViewTextBoxColumn";
            this.postsalaryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // postbossDataGridViewCheckBoxColumn
            // 
            this.postbossDataGridViewCheckBoxColumn.DataPropertyName = "Post_boss";
            this.postbossDataGridViewCheckBoxColumn.HeaderText = "Post_boss";
            this.postbossDataGridViewCheckBoxColumn.Name = "postbossDataGridViewCheckBoxColumn";
            this.postbossDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // postBindingSource
            // 
            this.postBindingSource.DataMember = "Post";
            this.postBindingSource.DataSource = this.dB15DataSet;
            // 
            // dB15DataSet
            // 
            this.dB15DataSet.DataSetName = "DB15DataSet";
            this.dB15DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // postTableAdapter
            // 
            this.postTableAdapter.ClearBeforeFill = true;
            // 
            // Post_id_box
            // 
            this.Post_id_box.Location = new System.Drawing.Point(572, 12);
            this.Post_id_box.Name = "Post_id_box";
            this.Post_id_box.Size = new System.Drawing.Size(140, 20);
            this.Post_id_box.TabIndex = 1;
            this.Post_id_box.Text = "Post id";
            // 
            // Department_id_box
            // 
            this.Department_id_box.Location = new System.Drawing.Point(572, 38);
            this.Department_id_box.Name = "Department_id_box";
            this.Department_id_box.Size = new System.Drawing.Size(140, 20);
            this.Department_id_box.TabIndex = 2;
            this.Department_id_box.Text = "Department id";
            // 
            // Post_name_box
            // 
            this.Post_name_box.Location = new System.Drawing.Point(572, 64);
            this.Post_name_box.Name = "Post_name_box";
            this.Post_name_box.Size = new System.Drawing.Size(140, 20);
            this.Post_name_box.TabIndex = 3;
            this.Post_name_box.Text = "Post name";
            // 
            // Post_salary_box
            // 
            this.Post_salary_box.Location = new System.Drawing.Point(572, 90);
            this.Post_salary_box.Name = "Post_salary_box";
            this.Post_salary_box.Size = new System.Drawing.Size(140, 20);
            this.Post_salary_box.TabIndex = 4;
            this.Post_salary_box.Text = "Post salary";
            // 
            // Post_boss_checkBox
            // 
            this.Post_boss_checkBox.AutoSize = true;
            this.Post_boss_checkBox.Location = new System.Drawing.Point(573, 117);
            this.Post_boss_checkBox.Name = "Post_boss_checkBox";
            this.Post_boss_checkBox.Size = new System.Drawing.Size(72, 17);
            this.Post_boss_checkBox.TabIndex = 5;
            this.Post_boss_checkBox.Text = "Post boss";
            this.Post_boss_checkBox.UseVisualStyleBackColor = true;
            // 
            // Insert_button
            // 
            this.Insert_button.Location = new System.Drawing.Point(572, 140);
            this.Insert_button.Name = "Insert_button";
            this.Insert_button.Size = new System.Drawing.Size(140, 50);
            this.Insert_button.TabIndex = 6;
            this.Insert_button.Text = "Добавить";
            this.Insert_button.UseVisualStyleBackColor = true;
            this.Insert_button.Click += new System.EventHandler(this.Insert_button_Click);
            // 
            // Update_button
            // 
            this.Update_button.Location = new System.Drawing.Point(573, 196);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(140, 50);
            this.Update_button.TabIndex = 7;
            this.Update_button.Text = "Изменить";
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // Delete_button
            // 
            this.Delete_button.Location = new System.Drawing.Point(572, 252);
            this.Delete_button.Name = "Delete_button";
            this.Delete_button.Size = new System.Drawing.Size(140, 50);
            this.Delete_button.TabIndex = 8;
            this.Delete_button.Text = "Удалить";
            this.Delete_button.UseVisualStyleBackColor = true;
            this.Delete_button.Click += new System.EventHandler(this.Delete_button_Click);
            // 
            // Post_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 319);
            this.Controls.Add(this.Delete_button);
            this.Controls.Add(this.Update_button);
            this.Controls.Add(this.Insert_button);
            this.Controls.Add(this.Post_boss_checkBox);
            this.Controls.Add(this.Post_salary_box);
            this.Controls.Add(this.Post_name_box);
            this.Controls.Add(this.Department_id_box);
            this.Controls.Add(this.Post_id_box);
            this.Controls.Add(this.dataGridView_post);
            this.Name = "Post_form";
            this.Text = "Должность";
            this.Load += new System.EventHandler(this.Post_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_post)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.postBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dB15DataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_post;
        private DB15DataSet dB15DataSet;
        private System.Windows.Forms.BindingSource postBindingSource;
        private DB15DataSetTableAdapters.PostTableAdapter postTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn postidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn postnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn postsalaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn postbossDataGridViewCheckBoxColumn;
        private System.Windows.Forms.TextBox Post_id_box;
        private System.Windows.Forms.TextBox Department_id_box;
        private System.Windows.Forms.TextBox Post_name_box;
        private System.Windows.Forms.TextBox Post_salary_box;
        private System.Windows.Forms.CheckBox Post_boss_checkBox;
        private System.Windows.Forms.Button Insert_button;
        private System.Windows.Forms.Button Update_button;
        private System.Windows.Forms.Button Delete_button;
    }
}