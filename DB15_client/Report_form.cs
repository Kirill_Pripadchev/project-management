﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB15_client
{
    public partial class Report_form : Form
    {
        public Report_form()
        {
            InitializeComponent();
        }

        private void Report_form_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "DB15DataSet.Worker". При необходимости она может быть перемещена или удалена.
            this.WorkerTableAdapter.Fill(this.DB15DataSet.Worker);

            this.reportViewer1.RefreshReport();
        }
    }
}
